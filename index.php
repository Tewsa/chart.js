<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="TRautio">
        <title>Ohjelmoijakysely</title>
        <link href="css/style.css" rel="stylesheet">
        <link rel="icon" href="favicon.ico">
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="index.php" id="curr">Kysely</a></li>
                <li><a href="count.html">Vastausten määrä</a></li>                
                <li><a href="record.html">Tulokset</a></li>
                <li><a href="avg.html">Keskiarvot</a></li>
            </ul>
        </nav>
        <h1>Ohjelmoijakysely</h1>
        <?php
        // Avataan tietokantayhteys.
        $tietokanta = new PDO('mysql:host=localhost;dbname=kysely;charset=utf8', 'root', '');
        //Oletuksena PDO ei näytä mahdollisia virheitä, joten asetetaan "virhemoodi" päälle.
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            try {
                // Luetaan tiedot lomakkeelta.
                $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
                $ika = filter_input(INPUT_POST, 'ika', FILTER_SANITIZE_NUMBER_INT);
                $skp = filter_input(INPUT_POST, 'skp', FILTER_SANITIZE_NUMBER_INT);
                $kokemus = filter_input(INPUT_POST, 'kokemus', FILTER_SANITIZE_NUMBER_INT);
                $yleinen = filter_input(INPUT_POST, 'yleinen', FILTER_SANITIZE_NUMBER_INT);
                $frontend = filter_input(INPUT_POST, 'frontend', FILTER_SANITIZE_NUMBER_INT);
                $backend = filter_input(INPUT_POST, 'backend', FILTER_SANITIZE_NUMBER_INT);
                $nmobile = filter_input(INPUT_POST, 'nmobile', FILTER_SANITIZE_NUMBER_INT);
                $hmobile = filter_input(INPUT_POST, 'hmobile', FILTER_SANITIZE_NUMBER_INT);
                $rdatabase = filter_input(INPUT_POST, 'rdatabase', FILTER_SANITIZE_NUMBER_INT);
                $nosql = filter_input(INPUT_POST, 'nosql', FILTER_SANITIZE_NUMBER_INT);


                if ($id == 0) {
                    $kysely = $tietokanta->prepare("INSERT INTO ohjelmoija (ika, skp, kokemus, yleinen, frontend, backend, nmobile, hmobile, rdatabase, nosql) "
                            . " VALUES (:ika, :skp, :kokemus, :yleinen, :frontend, :backend, :nmobile, :hmobile, :rdatabase, :nosql)");
                }

                $kysely->bindValue(':ika', $ika, PDO::PARAM_INT);
                $kysely->bindValue(':skp', $skp, PDO::PARAM_INT);
                $kysely->bindValue(':kokemus', $kokemus, PDO::PARAM_INT);
                $kysely->bindValue(':yleinen', $yleinen, PDO::PARAM_INT);
                $kysely->bindValue(':frontend', $frontend, PDO::PARAM_INT);
                $kysely->bindValue(':backend', $backend, PDO::PARAM_INT);
                $kysely->bindValue(':nmobile', $nmobile, PDO::PARAM_INT);
                $kysely->bindValue(':hmobile', $hmobile, PDO::PARAM_INT);
                $kysely->bindValue(':rdatabase', $rdatabase, PDO::PARAM_INT);
                $kysely->bindValue(':nosql', $nosql, PDO::PARAM_INT);


                // Suoritetaan kysely ja tarkastetaan samalla mahdollinen virhe.
                if ($kysely->execute()) {
                    print '<br>';
                    print('<p>Ohjelmoijan tiedot tallennettu! <a href="count.html">Kyselyyn vastanneet..</a></p>');
                    $id = $tietokanta->lastInsertId();
                } else {
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }
            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
            }
        }
        ?>
        <div id="one">
            <form action="<?php print $_SERVER['PHP_SELF']; ?>" method="post">
                <h4>Yleistiedot</h4>
                <div>
                    <label>Ikä:</label>
                    <input name="ika" type="number" required="" max="65" min="15"><span id="arvo">Arvoalue: 15-65</span>
                </div>
                <div>
                    <label>Sukupuoli:</label><br>
                    <input type="radio" name="skp" value="1" checked=""> Nainen<br>
                    <input type="radio" name="skp" value="2"> Mies<br>
                    <input type="radio" name="skp" value="3"> Muu
                </div>
                <div>
                    <label>Kokemus vuosissa:</label>
                    <input name="kokemus" type="number" required="" max="50" min="0"><span id="arvo">Arvoalue: 0-50</span>
                </div>
                <h4>Arvioi ohjelmointiosaamistasi</h4>
                <div>
                    <p>Ohjelmointi yleisellä tasolla</p>
                    <input type="radio" name="yleinen" value="5" required=""> Erittäin hyvä<br>
                    <input type="radio" name="yleinen" value="4" required=""> Hyvä<br>
                    <input type="radio" name="yleinen" value="3" required=""> Tyydyttävä<br>
                    <input type="radio" name="yleinen" value="2" required=""> Välttävä<br>
                    <input type="radio" name="yleinen" value="1" required=""> Heikko
                </div>
                <div>
                    <p>Web frontend</p>
                    <input type="radio" name="frontend" value="5" required=""> Erittäin hyvä<br>
                    <input type="radio" name="frontend" value="4" required=""> Hyvä<br>
                    <input type="radio" name="frontend" value="3" required=""> Tyydyttävä<br>
                    <input type="radio" name="frontend" value="2" required=""> Välttävä<br>
                    <input type="radio" name="frontend" value="1" required=""> Heikko
                </div>
                <div>
                    <p>Web backend</p>
                    <input type="radio" name="backend" value="5" required=""> Erittäin hyvä<br>
                    <input type="radio" name="backend" value="4" required=""> Hyvä<br>
                    <input type="radio" name="backend" value="3" required=""> Tyydyttävä<br>
                    <input type="radio" name="backend" value="2" required=""> Välttävä<br>
                    <input type="radio" name="backend" value="1" required=""> Heikko
                </div>
                <div>
                    <p>Natiivi mobiili</p>
                    <input type="radio" name="nmobile" value="5" required=""> Erittäin hyvä<br>
                    <input type="radio" name="nmobile" value="4" required=""> Hyvä<br>
                    <input type="radio" name="nmobile" value="3" required=""> Tyydyttävä<br>
                    <input type="radio" name="nmobile" value="2" required=""> Välttävä<br>
                    <input type="radio" name="nmobile" value="1" required=""> Heikko
                </div>
                <div>
                    <p>Hybridi mobiili</p>
                    <input type="radio" name="hmobile" value="5" required=""> Erittäin hyvä<br>
                    <input type="radio" name="hmobile" value="4" required=""> Hyvä<br>
                    <input type="radio" name="hmobile" value="3" required=""> Tyydyttävä<br>
                    <input type="radio" name="hmobile" value="2" required=""> Välttävä<br>
                    <input type="radio" name="hmobile" value="1" required=""> Heikko
                </div>
                <div>
                    <p>Relaatiotietokannat</p>
                    <input type="radio" name="rdatabase" value="5" required=""> Erittäin hyvä<br>
                    <input type="radio" name="rdatabase" value="4" required=""> Hyvä<br>
                    <input type="radio" name="rdatabase" value="3" required=""> Tyydyttävä<br>
                    <input type="radio" name="rdatabase" value="2" required=""> Välttävä<br>
                    <input type="radio" name="rdatabase" value="1" required=""> Heikko
                </div>
                <div>
                    <p>NoSQL-tietokannat</p>
                    <input type="radio" name="nosql" value="5" required=""> Erittäin hyvä<br>
                    <input type="radio" name="nosql" value="4" required=""> Hyvä<br>
                    <input type="radio" name="nosql" value="3" required=""> Tyydyttävä<br>
                    <input type="radio" name="nosql" value="2" required=""> Välttävä<br>
                    <input type="radio" name="nosql" value="1" required=""> Heikko
                </div>
                <button>Tallenna</button>
                <button type="button" onclick="window.location = 'index.php';">Tyhjennä</button>
            </form>
        </div>
        <div id="two">
            <img src="img/Colibri.png">
            <p></p>
            <img src="img/Colibritwo.png">
        </div>
        <footer>Tessa Rautio 2017K</footer>
    </body>
</html>
