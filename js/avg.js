$(document).ready(function () {
    $.ajax({
        url: "http://localhost/Chartjs_Rautio/avg.php",
        method: "GET",
        success: function (data) {
            console.log(data);

            var avg_ika = [];
            var avg_kokemus = [];
            var avg_yleinen = [];
            var avg_frontend = [];
            var avg_backend = [];
            var avg_nmobile = [];
            var avg_hmobile = [];
            var avg_rdatabase = [];
            var avg_nosql = [];

            for (var i in data) {
                avg_ika.push(data[i].avg_ika);
                avg_kokemus.push(data[i].avg_kokemus);
                avg_yleinen.push(data[i].avg_yleinen);
                avg_frontend.push(data[i].avg_frontend);
                avg_backend.push(data[i].avg_backend);
                avg_nmobile.push(data[i].avg_nmobile);
                avg_hmobile.push(data[i].avg_hmobile);
                avg_rdatabase.push(data[i].avg_rdatabase);
                avg_nosql.push(data[i].avg_nosql);
            }

            var avg_ika = {
                labels: "Iän keskiarvo",
                datasets: [
                    {
                        label: 'Iän keskiarvo',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: avg_ika
                    }
                ]
            };

            var ctxi = $("#avg_ika");

            var avg_ika = new Chart(ctxi, {
                type: 'bar',
                data: avg_ika
//                options: {
//                    scales: {
//                        yAxes: [{
//                                ticks: {
//                                    max: 20,
//                                    min: 0,
//                                    stepSize: 2
//                                }
//                            }]
//                    }
//                }
            });


            var avg_kokemus = {
                labels: avg_kokemus,
                datasets: [
                    {
                        label: 'Kokemus',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: avg_kokemus
                    }
                ]
            };

            var ctxk = $("#avg_kokemus");

            var avg_kokemus = new Chart(ctxk, {
                type: 'bar',
                data: avg_kokemus
            });
                        
            var avg_kaikki = {
                labels: avg_kokemus,
                datasets: [
                    {
                        label: 'Kokemukset',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: [avg_frontend, avg_backend, avg_nmobile, avg_hmobile, avg_rdatabase, avg_nosql]
                    }
                ]
            };

            var ctxa = $("#avg_kokemus");

            var avg_kaikki = new Chart(ctxa, {
                type: 'bar',
                data: [avg_frontend, avg_backend, avg_nmobile, avg_hmobile, avg_rdatabase, avg_nosql]
            });
                        


        },
        error: function (data) {
            console.log(data);
        }
    });
});