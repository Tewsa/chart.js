$(document).ready(function () {
    $.ajax({
        url: "http://localhost/Chartjs_Rautio/record.php", //tämä eri koulussa kuin kotona! /localhost/Chartjs_TRautio
        method: "GET",
        success: function (data) {
            console.log(data);

            var ika = [0, 0, 0];
            var skp = [0, 0, 0];
            var kokemus = [0, 0, 0, 0];
            var yleinen = [0, 0, 0, 0, 0];
            var frontend = [0, 0, 0, 0, 0];
            var backend = [0, 0, 0, 0, 0];
            var nmobile = [0, 0, 0, 0, 0];
            var hmobile = [0, 0, 0, 0, 0];
            var rdatabase = [0, 0, 0, 0, 0];
            var nosql = [0, 0, 0, 0, 0];
            var arvot = [1, 2, 3, 4, 5];

            for (var i in data) {
                if (parseInt(data[i].ika) <= 30) {
                    ika[0]++;
                } else if ((parseInt(data[i].ika) > 30) && (parseInt(data[i].ika) <= 50)) {
                    ika[1]++;
                } else if (parseInt(data[i].ika) > 51) {
                    ika[2]++;
                }

                if (parseInt(data[i].skp) === 1) {
                    skp[0]++;
                } else if (parseInt(data[i].skp) === 2) {
                    skp[1]++;
                } else {
                    skp[2]++;
                }

                if (parseInt(data[i].kokemus)<=5) {
                    kokemus[0]++;
                } else if (parseInt(data[i].kokemus)<=10 && parseInt(data[i].kokemus)>5) {
                    kokemus[1]++;
                } else if (parseInt(data[i].kokemus)<=15 && parseInt(data[i].kokemus)>10) {
                    kokemus[2]++;
                } else {
                    kokemus[3]++;
                }
                
                yleinen[parseInt(data[i].yleinen-1)]++;
                frontend[parseInt(data[i].frontend-1)]++;
                backend[parseInt(data[i].backend-1)]++;
                nmobile[parseInt(data[i].nmobile-1)]++;
                hmobile[parseInt(data[i].hmobile-1)]++;
                rdatabase[parseInt(data[i].rdatabase-1)]++;
                nosql[parseInt(data[i].nosql-1)]++;

            }





            var ika = {
                labels: ["15-30", "31-50", "51-56"],
                datasets: [
                    {
                        backgroundColor: [
                            "#ffb3d1",
                            "#b3ecff",
                            "#c6ffb3"
                        ],
                        data: ika
                    }
                ]
            };

            var skp = {
                labels: ["Nainen", "Mies", "Muu"],
                datasets: [
                    {
                        backgroundColor: [
                            "#ffb3d1",
                            "#b3ecff",
                            "#c6ffb3"
                        ],
                        data: skp
                    }
                ]
            };


            var kokemus = {
                labels: ["0-5", "6-10", "11-15", "yli 15"],
                datasets: [
                    {
                        backgroundColor: [
                            "#ffb3d1",
                            "#b3ecff",
                            "#c6ffb3",
                            "#ffecb3"
                        ],
                        data: kokemus
                    }
                ]
            };

            var yleinen = {
                labels: arvot,
                datasets: [
                    {
                        label: 'Kokemus yleisellä tasolla',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: yleinen
                    }
                ]
                
            };

            var frontend = {
                labels: arvot,
                datasets: [
                    {
                        label: 'Web frontend',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: frontend
                    }
                ]
            };

            var backend = {
                labels: arvot,
                datasets: [
                    {
                        label: 'Web backend',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: backend
                    }
                ]
            };

            var nmobile = {
                labels: arvot,
                datasets: [
                    {
                        label: 'Natiivi mobiili',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: nmobile //[0,2,4,6,8,10,12,14,16,18,20]
                    }
                ]
            };

            var hmobile = {
                labels: arvot,
                datasets: [
                    {
                        label: 'Hybridi mobiili',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: hmobile
                    }
                ]
            };

            var rdatabase = {
                labels: arvot,
                datasets: [
                    {
                        label: 'Relaatiotietokannat',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: rdatabase //[0,2,4,6,8,10,12,14,16,18,20]
                    }
                ]
            };

            var nosql = {
                labels: arvot,
                datasets: [
                    {
                        label: 'NoSQL',
                        backgroundColor: '#c6ffb3',
                        borderColor: '#b3ffb3',
                        hoverBackgroundColor: '#b3ffb3',
                        hoverBorderColor: '#c6ffb3',
                        data: nosql //[0,2,4,6,8,10,12,14,16,18,20]
                    }
                ]
            };

            var ctxi = $("#ika");
            var ctxs = $("#skp");
            var ctxk = $("#kokemus");
            var ctxy = $("#yleinen");
            var ctxf = $("#frontend");
            var ctxb = $("#backend");
            var ctxn = $("#nmobile");
            var ctxh = $("#hmobile");
            var ctxr = $("#rdatabase");
            var ctxno = $("#nosql");

            var ika = new Chart(ctxi, {
                type: 'doughnut',
                data: ika
            });

            var skp = new Chart(ctxs, {
                type: 'pie',
                data: skp
            });

            var kokemus = new Chart(ctxk, {
                type: 'doughnut',
                data: kokemus
            });

            var yleinen = new Chart(ctxy, {
                type: 'bar',
                data: yleinen,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: 20,
                                min: 0,
                                stepSize: 2
                            }
                        }]
                    }
                }
            });

            var frontend = new Chart(ctxf, {
                type: 'bar',
                data: frontend,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: 20,
                                min: 0,
                                stepSize: 2
                            }
                        }]
                    }
                }
            });

            var backend = new Chart(ctxb, {
                type: 'bar',
                data: backend,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: 20,
                                min: 0,
                                stepSize: 2
                            }
                        }]
                    }
                }
            });

            var nmobile = new Chart(ctxn, {
                type: 'bar',
                data: nmobile,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: 20,
                                min: 0,
                                stepSize: 2
                            }
                        }]
                    }
                }
            });

            var hmobile = new Chart(ctxh, {
                type: 'bar',
                data: hmobile,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: 20,
                                min: 0,
                                stepSize: 2
                            }
                        }]
                    }
                }
            });

            var rdatabase = new Chart(ctxr, {
                type: 'bar',
                data: rdatabase,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: 20,
                                min: 0,
                                stepSize: 2
                            }
                        }]
                    }
                }
            });

            var nosql = new Chart(ctxno, {
                type: 'bar',
                data: nosql,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: 20,
                                min: 0,
                                stepSize: 2
                            }
                        }]
                    }
                }
            });



        },
        
        error: function (data) {
            console.log(data);
        }
    });
});