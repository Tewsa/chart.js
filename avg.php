<?php
//setting header to json
header('Content-Type: application/json');

//database
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'kysely');

//get connection
$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

if(!$mysqli){
	die("Connection failed: " . $mysqli->error);
}

//query to get data from the table
//$query = sprintf("SELECT ika, skp, kokemus, yleinen, frontend, backend, nmobile, hmobile, rdatabase, nosql FROM ohjelmoija");

$query = sprintf("SELECT AVG(ika) AS avg_ika, "
        . "AVG(kokemus) AS avg_kokemus, "
        . "AVG(yleinen) AS avg_yleinen, "
        . "AVG(frontend) AS avg_frontend, "
        . "AVG(backend) AS avg_backend, "
        . "AVG(nmobile) AS avg_nmobile, "
        . "AVG(hmobile) AS avg_hmobile, "
        . "AVG(rdatabase) AS avg_rdatabase, "
        . "AVG(nosql) AS avg_nosql "
        . "FROM ohjelmoija");
//execute query
$result = $mysqli->query($query);

//loop through the returned data
$data = array();
foreach ($result as $row) {
    $data[] = $row;
}

//free memory associated with result
$result->close();

//close connection
$mysqli->close();

//now print the data
print json_encode($data);