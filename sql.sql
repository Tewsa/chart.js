/**
 * Author:  Tewsa
 * Created: 31.1.2017
 */

/* Tuhotaan tietokanta, jos löytyy. Jos ohjelma ei ole vielä käytössä, on tämä
helppo tapa, jos tietokantaan tulee muutoksia kehitystyön aikana. */
drop database if exists kysely;

/* Luodaan tietokanta uudestaan. */
create database kysely;

/* Alla olevat lauseet suoritetaan äsken luotuun tietokantaan. */
use kysely;

/* Luodaan ohjelmoija-taulu.*/
create table ohjelmoija(
    id int unsigned primary key auto_increment,
    ika tinyint unsigned not null,
    skp tinyint not null, /*1=nainen, 2=mies 3=muu*/
    kokemus tinyint not null,
    yleinen tinyint not null,
    frontend tinyint not null,
    backend tinyint not null,
    nmobile tinyint not null,
    hmobile tinyint not null,
    rdatabase tinyint not null,
    nosql tinyint not null
    
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Lisään oletusarvoja/testidataa tietokantaan. */
insert into ohjelmoija (ika, skp, kokemus, yleinen, frontend, backend, nmobile, hmobile, rdatabase, nosql) 
values (23,1,3,4,5,4,3,3,3,4);
